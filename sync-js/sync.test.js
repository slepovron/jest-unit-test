const Lodash = require("./sync");

let _ = new Lodash(); // "let" cause we want to change variable value, and in global scope for using in all tests

describe("Lodash: compact", () => {
  let array; // what we use in next tests (and we create in global scope of describe function)
  // let result;

  beforeEach(() => {
    // some "hook" that will work before starting each test and ensuring us that we'll have original array value even if change it in one of tests
    array = [false, 42, 0, "", true, "string", null];
    result = [42, true, "string"];
  });

  afterAll(() => {
    let _ = new Lodash(); // just for example when to work beforeAll
  });

  test("Must be defined", () => {
    expect(_.compact).toBeDefined();
    expect(_.compact).not.toBeUndefined();
  });

  test("so now we edit array value", () => {
    array.push(...["newOne", "newTwo"]);
    expect(_.compact(array)).toContain("newOne");
    expect(_.compact(array)).toContain("newTwo");
  });

  // and thanks to beforeEach we didn't failing next test because array again have original value
  test("Remove falsy values from array", () => {
    // const array = [false, 42, 0, "", true, "string", null];
    // const result = [42, true, "string"];
    expect(_.compact(array)).toEqual(result);
  });

  test("array NOT contain falsy values after filtering", () => {
    // const array = [false, 42, 0, "", true, "string", null];
    expect(_.compact(array)).not.toContain(false);
    expect(_.compact(array)).not.toContain(0);
    expect(_.compact(array)).not.toContain("");
    expect(_.compact(array)).not.toContain(null);
  });
});

// before next test you need to understand .groupBy from Lodash library
describe("Lodash: groupBy", () => {
  test("defined or not defined", () => {
    expect(_.groupBy).toBeDefined();
    expect(_.groupBy).not.toBeUndefined();
  });

  test("group items in array by Math.floor", () => {
    const array = [2.2, 3.3, 3.5, 4.4, 2.7];
    const result = {
      2: [2.2, 2.7],
      3: [3.3, 3.5],
      4: [4.4],
    };
    expect(_.groupBy(array, Math.floor)).toEqual(result);
  });

  test("group items in array by length", () => {
    const array = ["one", "two", "string", "strong"];
    const result = {
      3: ["one", "two"],
      6: ["string", "strong"],
    };
    expect(_.groupBy(array, "length")).toEqual(result); //
  });

  test("if it NOT instance of global class Array", () => {
    expect(_.groupBy([], Math.floor)).not.toBeInstanceOf(Array); // whatever what exactly we sent in .groupBy it returned object to us - so we pass this test
  });
});
