class Lodash {
  compact(array) {
    return array.filter((value) => !!value); // !! - filtering falsy values, as ! - filtering truthful values
  }

  //more about .groupBy in Lodash library documentation
  groupBy(array, prop) {
    return array.reduce((accumulator, item) => {
      const key = typeof prop === "function" ? prop(item) : item[prop];
      if (!accumulator[key]) {
        accumulator[key] = [];
      }
      accumulator[key].push(item);
      return accumulator; //accumulator is not array
    }, {});
  }
}

module.exports = Lodash;
