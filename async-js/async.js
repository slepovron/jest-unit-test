class Ajax {
  static echo(data) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (data) {
          resolve(data);
        } else {
          reject(new Error("error"));
        }
      }, 50);
    });
  }
}

module.exports = Ajax;
