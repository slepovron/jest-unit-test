const { TestScheduler } = require("jest");
const Ajax = require("./async.js");

describe("Ajax: echo", () => {
  // for testing async functions we use ... async structures =)
  test("returning async fn value", async () => {
    const result = await Ajax.echo("some data");
    expect(result).toBe("some data");
  });

  test("returning fn value in promise structure", async () => {
    // for clearly informing Jest about "you must wait" we use "return"
    return Ajax.echo("some data").then((result) => {
      expect(result).toBe("some data");
    });
  });

  test("catching Error in promise structure", () => {
    return Ajax.echo().catch((error) => {
      expect(error).toBeInstanceOf(Error);
    });
  });

  test("catching Error in try/catch structure", async () => {
    try {
      await Ajax.echo();
    } catch (error) {
      expect(error.message).toBe("error");
    }
  });
});
