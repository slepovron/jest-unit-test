// part ".test." (or ".spec.") in this file name will be used as marker for Jest "work with this file"

// requiring function for testing
const { sum, myNull } = require("./basic.js");

// "describe" is global function from jest for combine various tests
describe("All tests for sum() values", () => {
  // "test" is global function of jest library
  test("Sum must be checked", () => {
    // "expect" is global function which expectation inner result
    expect(sum(1, 3)).toBe(4); // toBe is matcher for primitives
    expect(sum(1, 3)).toEqual(4); // toEqual is matcher for complicated objects
  });

  test("Sum must be... than other number", () => {
    expect(sum(1, 3)).toBeGreaterThan(3);
    expect(sum(1, 4)).toBeGreaterThanOrEqual(5);
    expect(sum(1, 3)).toBeLessThan(10);
    expect(sum(1, 4)).toBeLessThanOrEqual(5);
  });

  test("Sum must be... (for float values)", () => {
    expect(sum(0.1, 0.3)).toBeCloseTo(0.4);
  });
});

describe("Null testing", () => {
  test("Null must return false value Null", () => {
    expect(myNull()).toBe(null);
    expect(myNull()).toBeNull();
    expect(myNull()).toBeFalsy(); // null, undefined, '', 0
    expect(myNull()).toBeDefined();
    expect(myNull()).not.toBeTruthy();
    expect(myNull()).not.toBeUndefined();
  });
});
