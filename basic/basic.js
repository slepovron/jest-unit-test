function myTest(value) {
  return {
    // "toBe" is basic matcher from Jest
    toBe: (exp) => {
      if (value === exp) {
        console.log("Success!");
      } else {
        console.error(`${value} вщуы not equal ${exp}`);
      }
    },
  };
}

const sum = (a, b) => a + b;

const myNull = () => null;

// console.log(sum(41, 1));
// myTest(sum(41, 1)).toBe(42);

module.exports = { sum, myNull };
