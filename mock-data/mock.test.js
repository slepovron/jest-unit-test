const { TestScheduler } = require("jest");

const { map } = require("./mock");

describe("Map function", () => {
  let array;
  let fn;

  beforeEach(() => {
    array = [1, 2, 3, 5, 8];
    // below is mock (aka fake) function in Jest, that mean we made artifical func for testing some behave our tested func
    fn = jest.fn((x) => x ** 2);
    map(array, fn);
  });

  test("Callback was calling", () => {
    expect(fn).toBeCalled(); // fake method fn was called in testing function map
  });

  test("Callback was calling N times", () => {
    expect(fn).toBeCalledTimes(5); // fn was called 5 times, for each array's element
    expect(fn.mock.calls.length).toBe(5); // the same test with other syntax
  });

  test("Each element was pow^2", () => {
    expect(fn.mock.results[0].value).toBe(1); // result of N element == expected
    expect(fn.mock.results[1].value).toBe(4);
    expect(fn.mock.results[2].value).toBe(9);
    expect(fn.mock.results[3].value).toBe(25);
    expect(fn.mock.results[4].value).toBe(64);
  });

  test("Check mock function fn work", () => {
    fn.mockReturnValueOnce(100)
      .mockReturnValueOnce(300)
      .mockReturnValue("value");

    expect(fn()).toBe(100);
    expect(fn()).toBe(300);
    expect(fn()).toBe("value");
  });
});
